return


# In this task you will learn about automatic variables and how to use them

################ Section 1: Getting to know variables ###############

## Task 1.0: View all variables (and their values) in your session
Get-Variable


############


## Task 1.1: Filter out any variables that start with "PS"
#           -> These variables are set automatically by PowerShell
Get-Variable -Name "PS*"



############


## Task 1.2: Use of 'interesting' variables
$psEditor   

# This variable is used by VSCode to provide the integration between powershell and the editor
# we can actually interact with the editor using this variable:
$psEditor.Window.ShowInformationMessage("Hello from PowerShell!")   

# IMPORTANT: Close the window to continue.


############


## Task 1.3: Compare output of $Host

## $Host Variable
#
# The $Host variable gives you information about the current *host* where PowerShell is running


# IMPORTANT: The $Host.Version Variable was used in the past to get the Version of *PowerShell*.
#            -> This is not correct, as it gives the version of the *PowerShell Host*
# See:
powershell.exe -command '$Host.Version;$PSVersionTable.PSVersion'
pwsh.exe -command '$Host.Version;$PSVersionTable.PSVersion'
$Host.Version;$PSVersionTable.PSVersion



############


###### Task 1.4: Use of *useful* variables

## $PSVersionTable
# The $PSVersionTable variable gives you a lot of information about your current PowerShell session
$PSVersionTable

## $PSScriptRoot
# The PSScriptRoot Variable is a bit tricky -> it is set to the directory that the current script resides in
# BUT! as we are not actually executing a script, it is not set.
$PSScriptRoot

# To view the content, run the script "z_example_psscriptroot.ps1"
powershell.exe .\z_example_psscriptroot.ps1


############


###### Task 1.5: Sending an E-Mail with $PSEmailServer

## $PSEmailServer
# The $PSEmailServer variable is actually pretty selfexplanatory:
$PSEmailServer

# Set it once in your script and all E-Mails will be transferred via this server:
$PSEmailServer = "smtp0.ethz.ch"

# Now send the E-Mail
Send-MailMessage -To "<yourEmailhere>" -Subject "Hello From PowerShell $($PSVersionTable.PSVersion)" -Body "Hello Outlook!"

### Is... Variables
# PowerShell Core provides some variables to determine what environment you are currently on
Get-Variable -Name "is*"

# -> IsCoreCLR refers to the .NET Framework that PowerShell is running on (.NET Standard or .NET Core)

################ Section 2: Variable scoping ################

# All environment Variables are available with the Prefix $env:....
$env:USERNAME
$env:Path
$env:ProgramFiles
# ...

# Global Variables
# if you (for some reason) require variables, that are shared in all scripts, use global variables
$Global:MyGlobalVariable = 15

# access can be done by 
$MyGlobalVariable 
# or
$Global:MyGlobalVariable

# But please think twice if you actually need a global variable ;)
