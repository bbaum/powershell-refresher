# Task 1.0: Familiarize yourself with the available commands:
Get-Command -Module IAMClient

# Task 1.1: Initialize the Client -> Login
Initialize-IAMClient

# Task 1.2: Count the nr of personas of 'aurels' that start with "asc4"
#           Some starting code is provided
$aurels = Get-ETHPerson aurels
$Usernames = $aurels.usernames | Where-Object # ... complete here

# Verify results (should be 7)
$Usernames | Measure-Object


# Task 1.3: Create a group
#            -> In your admingroup (D-BIOL, ID-S4D, etc)
#            -> With a name that contains "test" somewhere (for others to see why this exists)
#            -> !! Exported to AD ONLY !!
#
#            Help: Get-Help New-ETHGroup -Examples
New-ETHGroup # ...



# Task 1.4: Add yourself and *your current user* to the group
#           Get-Help is not yet available :(
Add-ETHGroupMember -Identity <# ... #> -Members <# ... #>

# Verify results
Get-ADGroupMember -Identity # ... completehere
Get-ETHGroupMember -Identity # ... completehere

## Task 1.5: Remove your group again
#             -> Get-Help works!
Remove-ETHGroup











######################## SOLUTIONS
# SPOILER ALERT
# SPOILER ALERT
# SPOILER ALERT
# SPOILER ALERT
# SPOILER ALERT
# SPOILER ALERT
# SPOILER ALERT
# SPOILER ALERT
# SPOILER ALERT
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########


# Task 1.2
$aurels = Get-ETHPerson "aurels"
$usernames = $aurels.usernames | Where-Object username -like "asc4*" | Measure-Object


# Task 1.3
# Example provided:
New-ETHGroup -Name "biol-micro-testgroup-tobedel" -Description "testgroup" -Targets AD -AdminGroup "D-BIOL"


# Task 1.4
Add-ETHGroupMember -Identity "biol-micro-testgroup-tobedel" -Members $env:USERNAME,"aurels"

Get-ETHGroupMember -Identity "biol-micro-testgroup-tobedel"
Get-ADGroupMember -Identity "biol-micro-testgroup-tobedel"


# Task 1.5
Remove-ETHGroup -Name "biol-micro-testgroup-tobedel"
