# Hands On Lab 2
# Author: Aurel Schwitter

# this statement prevents the script from executing by itself (F5)
# IMPORTANT: place the cursor in the line and press F8 to execute the current line!
return;



# Task 1.1: Create a *sorted* table (by description) with names and *descriptions* of all 
#           computers that start with "7050-".
#           
#           I provided you some example code to get started,
#           but you have to complete it.
#
#           Help: PS> Get-Help Get-ADComputer -Parameter Properties
#           Hint: AD Property 'description'
#
Get-ADComputer -Filter {name -like "7050-*"} # ... complete

# Task 1.2: Retrieve all Users that start with "biolcourse-"
#           and store the result a variable
$Users = Get-ADUser # .... complete

# Task 1.3: Retrieve all members of the group "biol-isg-biolcourse-courseaccounts"
#           This group should contain all users from above, but one is missing
#           -> Find the missing one
$ActualMembers = # .... complete

# Hint: for Step 2, you need the Compare-Object CmdLet
Compare-Object # .... complete


## Task 1.4: A new IT employee has joined your team.
# Find out all groups that he needs to be added to
$UserA = Get-ADPrincipalGroupMembership -Identity "aurels" # let's pretend you are me
$UserB = Get-ADPrincipalGroupMembership -Identity "jgrand" # and joel is the new employee

# now that we have the data, lets find out which ID-* group Joel is not in:
# Your Code here:

Compare-Object # ... complete


# if all goes well, you should see 4 groups in difference, but only one that Joel is missing

# Understanding the Output:
#   "=>" Means 1st User is in group, 2nd User is not
#   "<=" Means 2nd User is in group, 1st user is not
# Examples:
Compare-Object @(1..3) @(1..4)
Compare-Object @(1..4) @(1,3,4)











######################## SOLUTIONS
# SPOILER ALERT
# SPOILER ALERT
# SPOILER ALERT
# SPOILER ALERT
# SPOILER ALERT
# SPOILER ALERT
# SPOILER ALERT
# SPOILER ALERT
# SPOILER ALERT
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########
#########

# Task 1.1
Get-ADComputer -Filter {name -like "7050-*"} -Properties Description | sort Description | select Name,Description

# Task 1.2
$Users = Get-ADUser -Filter {name -like "biolcourse-*"}

# Task 1.3
$ActualMembers = Get-ADGroupMember "biol-isg-biolcourse-courseaccounts"
Compare-Object -ReferenceObject $Users.Name  -DifferenceObject $ActualMembers.Name

# -> User "biolcourse-9" is missing

# Task 1.4
Compare-Object $UserA.Name $UserB.Name | Where-Object InputObject -like "ID-*"